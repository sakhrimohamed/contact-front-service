import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'contact-front-service';
  user = '';

  constructor(private keycloakService: KeycloakService, private auth:AuthService) {}
  
  logout() {
    this.keycloakService.logout();
  }

  ngOnInit(): void {
    this.user = this.auth.getLoggedUser()['preferred_username'];
  }

}
