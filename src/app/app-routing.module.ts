import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactsComponent } from './contacts/contacts.component';

const routes: Routes = [
  { path: 'contacts', component: ContactsComponent, canActivate: [AuthGuard]},
  { path: 'contacts/create', component: ContactCreateComponent, canActivate: [AuthGuard]},
  { path: 'contacts/:id', component: ContactDetailsComponent, canActivate: [AuthGuard]},
  { path: 'contacts/edit/:id', component: ContactEditComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: 'contacts', pathMatch: 'full'},
  { path: '**', redirectTo: 'contacts', pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
