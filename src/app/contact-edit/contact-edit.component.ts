import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,  Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { IContact } from '../_models/contact';
import { ContactService } from '../_services/contact.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit{
  pageTitle = 'Contact Edit';
  contact! : IContact;
  user = '';
  errorMessage = '';
  successMessage = '';
  contactForm!: FormGroup;
  submitted = false;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private contactService: ContactService,
              private auth:AuthService) {

  }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.user = this.auth.getLoggedUser()['preferred_username'];
    if (id) {
      this.getContact(this.user, id);
    }
    this.initFormGroup();
  }

  initFormGroup() {
    this.contactForm = this.formBuilder.group({
      firstName: new FormControl('', [ Validators.required, Validators.minLength(4)]),
      lastName: new FormControl('', [ Validators.required, Validators.minLength(4)]),
      email: new FormControl('', [ Validators.required, Validators.email]),
    });
  }

  getContact(userName : string, id: number): void {
    this.contactService.getcontactDetails(userName, id).subscribe({
      next: contact => {
        this.contact = contact;
        this.f.firstName.setValue(contact.firstName);
        this.f.lastName.setValue(contact.lastName);
        this.f.email.setValue(contact.email);
      },
      error: err => this.errorMessage = err
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactForm.controls; }

  onReset() {
    this.submitted = false;
    this.contactForm.reset();
  }
  onSubmit() { 
    console.log(this.contact);
    this.submitted = true;

    // stop here if form is invalid
    if (this.contactForm.invalid) {
        return;
    }
    this.update();
   }

   update() {
    this.contactService.updateContact(this.user, this.contact.id, this.contactForm.value).subscribe({
      next: contact => {
        this.contact = contact;
        this.f.firstName.setValue(contact.firstName);
        this.f.lastName.setValue(contact.lastName);
        this.f.email.setValue(contact.email);
        this.successMessage = 'Contact with code ' + this.contact.id + ' is successfully updated.';
      },
      error: err => this.errorMessage = err
    });
  }


  onBack(): void {
    this.router.navigate(['/contacts']);
  }

}
