import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,  Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { IContact } from '../_models/contact';
import { ContactService } from '../_services/contact.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit{
  pageTitle = 'Contact Details';
  contact : IContact| undefined;
  user = '';
  errorMessage = '';
  id! : number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private contactService: ContactService,
              private auth:AuthService) {
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.user = this.auth.getLoggedUser()['preferred_username'];
    if (this.id) {
      this.getContact(this.user, this.id);
    }
  }

  getContact(userName : string, id: number): void {
    this.contactService.getcontactDetails(userName, id).subscribe({
      next: contact => this.contact = contact,
      error: err => this.errorMessage = err
    });
  }

  onEdit(): void {
    this.router.navigate(['/contacts/edit/' + this.id]);
  }

  onBack(): void {
    this.router.navigate(['/contacts']);
  }
}
