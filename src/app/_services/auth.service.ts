import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakTokenParsed } from 'keycloak-js';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private keycloakService: KeycloakService) {}

  getUserName(): string {
    return this.keycloakService.getUsername();
  }
  logout(): void {
    this.keycloakService.logout();
  }
  getLoggedUser(): KeycloakTokenParsed {
    return this.keycloakService.getKeycloakInstance().idTokenParsed as KeycloakTokenParsed;
  }
}
