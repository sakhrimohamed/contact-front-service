import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, tap, throwError, map } from "rxjs";
import { environment } from 'src/environments/environment';

import { IContact } from '../_models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private productUrl = `${environment.serverUrl}/api/v1/users/`;

  constructor(private http: HttpClient) { }

  getcontacts(username : string): Observable<IContact[]> {
    return this.http.get<IContact[]>(this.productUrl+username+'/contacts')
      .pipe(
        tap(data => console.log('All: ', JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  getcontactDetails(username : string, contactId : number): Observable<IContact> {
    return this.http.get<IContact>(this.productUrl+username+'/contacts/'+ contactId)
      .pipe(
        tap(contact => console.log('contact Details: ', JSON.stringify(contact))),
        catchError(this.handleError)
      );
  }

  deleteContact(username : string, contactId : number): Observable<boolean> {
    return this.http.delete<boolean>(this.productUrl+username+'/contacts/'+ contactId)
      .pipe(
        tap(isdeleted => {
          if(isdeleted) {
            console.log('contact with code ' + contactId +" is successfully deleted.");
            return true;
          }
          return false;
        }),
        catchError(this.handleError)
      );
  }

  updateContact(username : string, contactId : number, contactForm: any): Observable<IContact> {
    console.log(contactForm);
    return this.http.put<IContact>(this.productUrl+username+'/contacts/'+ contactId, contactForm)
      .pipe(
        tap(contact => {
            console.log('contact with code ' + contactId +" is successfully updated.");
            return contact;
        }),
        catchError(this.handleError)
      );
  }

  createContact(username : string, contactForm: any): Observable<IContact> {
    return this.http.post<IContact>(this.productUrl+username+'/contacts', contactForm)
      .pipe(
        tap(contact => {
            console.log('contact with code is successfully created');
            return contact;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(() => errorMessage);
  }

}
