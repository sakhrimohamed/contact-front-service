import { Component, OnDestroy, OnInit } from '@angular/core';
import { ContactService } from '../_services/contact.service';
import { Subscription } from "rxjs";
import { IContact } from '../_models/contact';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit, OnDestroy {
  pageTitle = 'Contact List';
  user = '';
  sub!: Subscription;
  contacts: IContact[] = [];
  errorMessage = '';
  successMessage = '';

  constructor(private contactService: ContactService, private auth:AuthService ) {}

  ngOnInit(): void {
    this.user = this.auth.getLoggedUser()['preferred_username'];
    this.sub = this.contactService.getcontacts(this.user).subscribe({
      next: contacts => {
        this.contacts = contacts;
      },
      error: err => this.errorMessage = err
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  delete(contactId : number): void {
    this.sub = this.contactService.deleteContact(this.user, contactId).subscribe({
      next: isDeleted => {
        if(isDeleted) {
          this.contacts = this.contacts.filter(c => c.id!= contactId);
          this.successMessage = 'Contact with code ' + contactId + ' is successfully deleted.'
        } else {
          this.errorMessage = 'Could not delete Contact with code ' + contactId + ' .'
        }
      },
      error: err => this.errorMessage = err
    });
  }
}
