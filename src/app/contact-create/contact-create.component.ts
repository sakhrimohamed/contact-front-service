import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,  Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { IContact } from '../_models/contact';
import { ContactService } from '../_services/contact.service';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit{
  contact! : IContact;
  user = '';
  errorMessage = '';
  successMessage = '';
  contactForm!: FormGroup;
  submitted = false;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private contactService: ContactService,
              private auth:AuthService) {

  }

  ngOnInit(): void {
    this.user = this.auth.getLoggedUser()['preferred_username'];
    this.initFormGroup();
  }

  initFormGroup() {
    this.contactForm = this.formBuilder.group({
      firstName: new FormControl('', [ Validators.required, Validators.minLength(4)]),
      lastName: new FormControl('', [ Validators.required, Validators.minLength(4)]),
      email: new FormControl('', [ Validators.required, Validators.email]),
    });
  }
  get f() { return this.contactForm.controls; }

  onReset() {
    this.submitted = false;
    this.contactForm.reset();
  }

  onSubmit() { 
    console.log(this.contact);
    this.submitted = true;

    // stop here if form is invalid
    if (this.contactForm.invalid) {
        return;
    }
    this.create();
  }

  create() {
    this.contactService.createContact(this.user, this.contactForm.value).subscribe({
      next: contact => {
        this.contact = contact;
        this.f.firstName.setValue(contact.firstName);
        this.f.lastName.setValue(contact.lastName);
        this.f.email.setValue(contact.email);
        this.successMessage = 'Contact is successfully created.';
      },
      error: err => this.errorMessage = err
    });
  }

  onBack(): void {
    this.router.navigate(['/contacts']);
  }

}
